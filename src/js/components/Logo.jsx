import '../../css/components/Logo.css'

const Logo = () => 
  <h1 className="logo">i<span style={{color: "#7ce96c"}}>F</span><span style={{color: "#ffffff"}}>G</span><span style={{color: "#e99b6c"}}>C</span></h1>

export default Logo;